
import React, { Component } from 'react'
// Import React FilePond
import { FilePond, registerPlugin, File } from 'react-filepond';

// Import FilePond styles
import 'filepond/dist/filepond.min.css';

// Import the Image EXIF Orientation and Image Preview plugins
// Note: These need to be installed separately
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import { apiUrls, authenticationToken } from '../../config';

// Register the plugins
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

class ProfileImageSelector extends Component {
  constructor(props) {
    super(props);

    this.state = {
        // Set initial files
        files: []
    };
}

handleInit() {
    console.log('FilePond instance has initialised', this.pond);
}

render() {
    return (
        <div className="circle responsive-img">
            {/* Pass FilePond properties as attributes */}
            <FilePond ref={ref => this.pond = ref}
                      allowMultiple={false} 
                      server=
                      {
                          {
                              url: apiUrls.profileImageUpload,
                              process: {
                                  headers: {
                                    "Authorization" : `Bearer ${window.sessionStorage.getItem(authenticationToken)}`
                                  },
                              }
                          }
                      }
                      oninit={() => this.handleInit() }
                      onupdatefiles={(fileItems) => {
                          // Set current file objects to this.state
                          this.setState({
                              files: fileItems.map(fileItem => fileItem.file)
                          });
                      }}
                      accept="image/png, image/jpeg, image/gif"
                      >
                
                {/* Update current files  */}
                {this.state.files.map(file => (
                    <File key={file} src={file} origin="local" />
                ))}
                
            </FilePond>
            
        </div>
    );
}
}

export default ProfileImageSelector;