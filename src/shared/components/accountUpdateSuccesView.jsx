import React, { Component } from 'react';
import Banner from "../../shared/assets/crapappbanner.png";
import { routes } from '../../config';

class AccountUpdateSuccesView extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col s8 offset-s2">
                        <div className="card">
                            <div className="card-image">
                                <img src={Banner} className="banner" alt="logo crap app" />
                            </div>
                            <div className="card-content">
                                <div className="row">
                                <p>Your request has been successful. 
                                    <br />
                                    Your account has been updated</p>
                                </div>
                                <div className="card-action">
                                    <a className="waves-effect waves-light btn" href={routes.login}>Go to login</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AccountUpdateSuccesView;