import {Component, Fragment} from "react";
import { FieldControl, FieldGroup, FormBuilder, Validators } from 'react-reactive-form';
import {Redirect} from "react-router-dom";
import {routes} from "../../config";
import Banner from "../../shared/assets/crapappbanner.png";
import TextInput from "../../shared/components/textInput";
import {sendMailUsername} from "../../shared/services/usernameMail";
import React from "react";

class forgotUsernameMail extends Component{
    getMailForm = FormBuilder.group({
        email: ['', Validators.required ]
    });

    constructor(props) {
        super(props);

        this.state = {
            redirection: false
        }
    }

    render() {
        return (
            this.state.redirection ? <Redirect to={routes.confirmationView}/> :
                <Fragment>
                    <FieldGroup
                        control={this.getMailForm}
                        render={({invalid}) => (
                            <form>
                                <div className="container">
                                    <div className="row">
                                        <div className="col s8 offset-s2">
                                            <div className="card">
                                                <div className="card-image">
                                                    <img src={Banner} className="banner" alt="logo crap app"/>
                                                </div>
                                                <div className="card-content">
                                                    <div className="row">
                                                        <FieldControl
                                                            name="email"
                                                            render={TextInput}
                                                            meta={{label: "email"}}
                                                        />
                                                    </div>
                                                    <div className="card-action">
                                                        <a className="waves-effect waves-light btn"
                                                           disabled={invalid}
                                                           onClick={() => this.askUsername()}
                                                           id="ForgotUsernameButton"
                                                        >send</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        )}/>
                </Fragment>
        );
    }

    askUsername() {
        sendMailUsername(this.getMailForm.value).then((response) => {
            if (response.status === 200) {
                this.setState({redirection: true});
            } else if (response.status === 404) {
                alert('We cannot find this emailaddress in our database');
            }
        });
    }
}
export default forgotUsernameMail;