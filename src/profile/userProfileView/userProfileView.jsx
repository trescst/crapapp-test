import React, { Component } from 'react';

import GenderSelectionField from '../../shared/components/genderSelectionField';
import { routes, regex } from '../../config';
import { FormBuilder, Validators, FieldGroup, FieldControl } from 'react-reactive-form';
import { checkIfMatchingPassword } from '../../shared/services/password';
import TextInput from '../../shared/components/textInput';
import ChangePasswordFields from './changePasswordFields';
import { Redirect } from 'react-router-dom';
import { updateUserProfile, getCurrentUser, deleteUserProfile } from '../../shared/services/user';
import ProfileImageSelector from '../../shared/components/ProfileImage';

class UserProfileView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
            changesPassword: false,
            user: {
                emailAddress: '',
                username: '',
                highscore: '',
                gender: ''
            }
        }

        getCurrentUser().then((response) => {
            if (response.status === 200) {
                response.text().then((user) => {
                    this.setState({ user: JSON.parse(user) });
                })
            }
        });
    }

    profileForm = FormBuilder.group({
        email: [''],
        username: ['', Validators.required],
        gender: ['', Validators.required]
    });

    passwordForm = FormBuilder.group({
        oldPassword: ['', Validators.required],
        newPassword: ['', Validators.compose([
            Validators.required,
            Validators.pattern(regex.password)
        ])],
        confirmNewPassword: ['', Validators.compose([
            Validators.required,
            Validators.pattern(regex.password)
        ])],
    },
        {
            validators: checkIfMatchingPassword('newPassword', 'confirmNewPassword')
        });

    render() {
        return (
            this.state.redirect ? <Redirect to={routes.startScreen} /> :
                <div className="top">
                    <div className="row">
                        <div className="col s5 offset-s1">
                            <div className="card horizontal col s12">
                                <div className="card-stacked">
                                    <div className="card-content">
                                        <table className="col s12">
                                            <tbody className="col s12">
                                                <tr className="col s12">
                                                    <td className="col s6">
                                                        <ProfileImageSelector />
                                                    </td>
                                                    <td className="col s6">
                                                        <div className="row">
                                                            <label>{this.state.user.username.toUpperCase()}</label>
                                                        </div>
                                                        <div className="row">
                                                            <label>{this.state.user.highscore}</label>
                                                        </div>
                                                        <div className="row">
                                                            <a className="waves-effect waves-light btn col s12 redButton" onClick={() => this.delete()}>Delete</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <FieldGroup
                                control={this.profileForm}
                                render={() => (
                                    <div className="card col s12">
                                        <div className="card-content">
                                            <FieldControl
                                                name="username"
                                                render={TextInput}
                                                meta={{ label: "Username" }}
                                            />
                                            <FieldControl
                                                name="gender"
                                                render={GenderSelectionField}
                                                meta={{ label: "Gender" }}
                                            />
                                        </div>
                                    </div>
                                )}
                            />
                        </div>
                        <div className="col s4 offset-s1">
                            {this.state.changesPassword ?
                                <ChangePasswordFields
                                    passwordForm={this.passwordForm}
                                />
                                : null}
                            <div className="col s12 top">
                                {this.state.changesPassword ? null :
                                    <div className="row">
                                        <a className="waves-effect waves-light btn col s12 logout" onClick={() => { this.setState({ changesPassword: true }) }}>Change my password</a>
                                    </div>
                                }
                                <div className="row">
                                    <a className="waves-effect waves-light btn col s5 secondary" href={routes.startScreen}>cancel</a>
                                    <a className="waves-effect waves-light btn col s5 offset-s2 logout" onClick={() => this.update()}>save</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
        );
    }

    update() {
        this.fillform();
        updateUserProfile(this.profileForm.value, this.passwordForm.value)
            .then((response) => {
                if (response.status === 202) {
                    this.setState({ redirect: true })
                } else if (response.status === 409) {
                    alert('The chosen username is already in use');
                }
            })
    }

    delete() {
        if (window.confirm('Are you sure you want to delete your profile?')) {
            deleteUserProfile().then((response) => {
                if (response.status === 200) {
                    this.setState({ redirect: true });
                }
            })
        }
    }

    fillform() {
        this.profileForm.get('email').setValue(this.state.user.emailAddress);

        if (this.profileForm.get('username').value === '') {
            this.profileForm.get('username').setValue(this.state.user.username);
        }

        if (this.profileForm.get('gender').value === '') {
            this.profileForm.get('gender').setValue(this.state.user.gender);
        }
    }
}

export default UserProfileView;