import React from 'react';
import Loading from '../../shared/assets/crapappbanner.png';

const LoadingStartScreen = (props) => {
  return (
    <div className="container centerContainer">
    <div className="row">
        <div className="col s4 offset-s5">
            <img src={Loading} className="loadingImage" alt="loading"/>
        </div>
    </div>
    <div className="row">
        <div className="col s10 offset-s2">
            <p>We are retrieving your account info from the server please hold on.</p>
        </div>
    </div>
</div>
);
}

export default LoadingStartScreen;
