import React, { Component } from 'react';
import { createUser } from '../../shared/services/user';
import ErrorPage from '../../shared/components/errorPage';
import LoadingScreen from '../../shared/components/loadingScreen';
import RegisterConfirmationSucces from './registerConfirmationSucces';
import RegisterConfirmationTokenExpired from './registerConfirmationTokenExpired';


class RegisterConfirmationView extends Component {
    constructor(props) {
        super(props);

        this.state ={
            registration: 0
        }

        createUser().then((response) => {
            this.setState({registration: response.status});
        });
    }

    render() {
            return (
                this.state.registration === 0?
                <LoadingScreen />
                : this.state.registration === 200?
                <RegisterConfirmationSucces /> 
                : this.state.registration === 404?
                <RegisterConfirmationTokenExpired /> 
                : <ErrorPage />
            )
    }
}

export default RegisterConfirmationView;